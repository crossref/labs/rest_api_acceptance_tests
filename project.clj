(defproject rest-api-acceptance-tests "0.1.0"
  :description "Acceptance tests for ES Cayenne"
  :url "https://gitlab.com/crossref/rest_api_acceptance_tests"
  :main crossref.cayenne.acceptance-tests
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [cheshire "5.8.0"] 
                 [clj-http "3.10.0"]
                 [com.taoensso/timbre "3.4.0"]])
