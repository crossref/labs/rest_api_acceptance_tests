(ns crossref.cayenne.acceptance-tests
  (:require [cheshire.core :as json]
            [clj-http.client :as client]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set]
            [taoensso.timbre :as timbre :refer [info]]))

(defn ag [url query]
  (-> (str url "/" query)
      (client/get {:insecure? true})
      :body
      (json/parse-string true)
      :message))

(defn api-get [url query]
  (try
    (ag url query)
    (catch Exception e (ag url query))))

;; /works metadata

(def ignore-works-fields [:published :subject :ISBN :isbn-type])
(def ignore-works-content [:indexed :score :institution])

(defn compare-works-key [old-record new-record k]
  (cond
    (some #{k} ignore-works-fields)
    0
    
    (and (contains? old-record k) (not (contains? new-record k)))
    (do (info "missing in new:" k) 1)
    
    (and (not (contains? old-record k)) (contains? new-record k))
    (do (info "missing in old:" k) 1)
    
    (some #{k} ignore-works-content)
    0
    
    (and (= :reference k)
         (not= (count (:reference old-record)) (count (:reference new-record))))
    (do (info "differs ref counts:"
          (count (:reference old-record))
          (count (:reference new-record)))
      1)
    
    (= :reference k)
    (reduce
      +
      (map
        #(if (= %1 %2) 0 (do (info "differs ref:" %1 %2) 1))
        (:reference old-record)
        (:reference new-record)))
    
    (not= (old-record k) (new-record k))
    (do (info "differs:" k (old-record k) (new-record k)) 1)
    
    :else 0))

(defn modify-work-record [record]
  (-> record
      (update-in [:issn-type] set)
      (update-in [:relation] #(if (= (:cites %) []) (dissoc % :cites) %))))

(defn compare-works-metadata [old-record new-record]
  (let [keys (set (concat (keys old-record) (keys new-record)))
        old-record (modify-work-record old-record)
        new-record (modify-work-record new-record)
        errors (map (partial compare-works-key old-record new-record) keys)]
    (reduce + errors)))

;; /members metadata

(def ignore-members-content [:last-status-check-time])

(defn clean-coverage [coverage]
  (if (map? (second (first coverage)))
    (->> coverage
         (map #(vector (first %) (clean-coverage (second %))))
         (into {}))
    (->> coverage
         (#(dissoc % :last-status-check-time))
         (map #(vector (first %) (if (nil? (second %)) {} (float (second %)))))
         (into {}))))

(defn modify-member-record [record]
  (-> record
      (update-in [:names] set)
      (update-in [:breakdowns :dois-by-issued-year] set)
      (update-in [:coverage] clean-coverage)
      (update-in [:coverage-type] clean-coverage)))

;; /journals metadata

(def ignore-journals-content [:last-status-check-time :subjects])

(defn modify-journal-record [record]
  (-> record
      (update-in [:ISSN] set)
      (update-in [:issn-type] set)
      (update-in [:breakdowns :dois-by-issued-year] set)
      (update-in [:coverage] clean-coverage)
      (update-in [:coverage-type] clean-coverage)))

;; /funders metadata

(def ignore-funders-content [])

(defn modify-funder-record [record]
  (-> record
      (update-in [:hierarchy-names] dissoc :more)
      (update-in [:descendants] set)))

;; generic metadata

(defn compare-metadata [old-record new-record ignore-content
                        & {:keys [modify-record] :or {modify-record identity}}]
  (let [keys (set (concat (keys old-record) (keys new-record)))
        old-record (modify-record old-record)
        new-record (modify-record new-record)
        scores (map #(cond
                       (and (contains? old-record %) (not (contains? new-record %)))
                       (do (info "missing in new:" %) 1)
                       
                       (and (not (contains? old-record %)) (contains? new-record %))
                       (do (info "missing in old:" %) 1)
                       
                       (some #{%} ignore-content)
                       0
                       
                       (not= (old-record %) (new-record %))
                       (do (info "differs:" % (old-record %) (new-record %)) 1)
                       
                       :else 0) keys)]
    (reduce + scores)))

(defn process-records [filename route compare-fun & args]
  (with-open [rdr (io/reader filename)]
    (let [errors (atom 0)
          ids (atom 0)]
      (doseq [id (line-seq rdr)]
        (info "id:" id @ids)
        (swap! ids inc)
        (let [old-record (api-get (System/getenv "OLD_URL") (str route "/" id))
              new-record (api-get (System/getenv "NEW_URL") (str route "/" id))
              error-count (apply (partial compare-fun old-record new-record) args)]
          (swap! errors + error-count)))
      (info route ":" @ids)
      (info "Errors:" @errors)
      (info "Errors per " route ":" (float (/ @errors @ids))))))

;; queries

(defn compare-approx [old-n new-n]
  (if (or (nil? old-n) (nil? new-n))
    (or (not (nil? old-n)) (not (nil? new-n)))
    (let [diff (Math/abs (- old-n new-n))
          change (float (/ diff (+ 0.001 old-n)))]
      (> change 0.01))))

(defn compare-totals [old-results new-results]
  (let [old-total (:total-results old-results)
        new-total (:total-results new-results)]
    (if (compare-approx old-total new-total)
      (do
        (info "Totals differ:" old-total new-total)
        1)
      0)))

(defn route-id [url]
  (cond
    (str/includes? url "works?") :DOI
    (str/includes? url "members?") :id
    (str/includes? url "funders?") :id
    (str/includes? url "licenses?") :URL
    (str/includes? url "journals?") :title
    :else nil))

(defn compare-first-n [old-results new-results id]
  (if id
    (let [ns [1 5 20]
          ns-thresholds [1 4 15]
          old-ids (->> old-results :items (map id))
          new-ids (->> new-results :items (map id))
          compare-n (fn [n min-n]
                      (let [old-ids-n (->> old-ids (take n) set)
                            new-ids-n (->> new-ids (take n) set)
                            ids-intersection (set/intersection old-ids-n new-ids-n)]
                        (cond
                          (or (not= (count old-ids-n) n) (not= (count new-ids-n) n))
                          0
                          
                          (< (count ids-intersection) min-n)
                          (do
                            (info "First" n "differ:" (count ids-intersection) old-ids-n new-ids-n)
                            1)
                          
                          :else
                          0)))]
      (->> (map compare-n ns ns-thresholds)
           (reduce +)))
    0))

(defn compare-facets [old-results new-results]  
  (let [facets (set/union (set (keys (:facets old-results)))
                          (set (keys (:facets new-results))))
        compare-facet (fn [facet]
                        (let [old-facets (-> old-results :facets facet :values)
                              new-facets (-> new-results :facets facet :values)
                              facet-keys (set/union (set (keys old-facets))
                                                    (set (keys new-facets)))]
                          (reduce +
                            (map
                              #(if (compare-approx (% old-facets) (% new-facets))
                                 (do
                                   (info "Facets differ:" % (% old-facets) (% new-facets))
                                   1)
                                 0)
                              facet-keys))))]
    (->> (map compare-facet facets)
         (reduce +))))

(defn process-queries [filename]
  (with-open [rdr (io/reader filename)]
    (let [errors (atom 0)
          queries (atom 0)]
      (doseq [query (line-seq rdr)]
        (info "query:" query @queries)
        (swap! queries inc)
        (let [old-results (api-get (System/getenv "OLD_URL") query)
              new-results (api-get (System/getenv "NEW_URL") query)]
          (swap! errors + (compare-totals old-results new-results))
          (when (str/includes? query "query")
            (swap! errors + (compare-first-n old-results new-results (route-id query))))
          (when (str/includes? query "facet")
            (swap! errors + (compare-facets old-results new-results)))))
      (info "Queries:" @queries)
      (info "Errors:" @errors)
      (info "Errors per query:" (float (/ @errors @queries))))))

;; ---

(defn -main [& args]
  (when (some #{"works_metadata"} args)
    (process-records
      "data/content-type-dois.txt"
      "works"
      compare-works-metadata))
  
  (when (some #{"members_metadata"} args)
    (process-records
      "data/sample-member-ids.txt"
      "members"
      compare-metadata
      ignore-members-content
      :modify-record modify-member-record))
  
  (when (some #{"journals_metadata"} args)
    (process-records
      "data/sample-journals.txt"
      "journals"
      compare-metadata
      ignore-journals-content
      :modify-record modify-journal-record))
  
  (when (some #{"funders_metadata"} args)
    (process-records
      "data/sample-funders.txt"
      "funders"
      compare-metadata
      ignore-funders-content
      :modify-record modify-funder-record))
 
  (when (some #{"queries"} args)
    (process-queries "data/queries.txt")))

